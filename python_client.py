import zeep
from lxml import etree as ET

#Location of wsdl
#wsdl = 'https://selectedtopickmutnb2018.azurewebsites.net/WebService.asmx?WSDL'
wsdl = 'http://localhost:50735/WebService.asmx?WSDL'
client = zeep.Client(wsdl=wsdl)

#print(client.service.GetDeliveryData())
#print(client.service.GetOnDeliveringData())
print(client.service.GetDeliveredData())
#client.service.AddDeliveryData("Thanut Suwannawong2","11/50 Bangkok",1.5,"Delivering")
#client.service.Delivered("Thanut Suwannawong2")


#node = client.create_message(client.service, 'GetStudentData', _soapheaders=)
#tree = ET.ElementTree(node)
#tree.write('test.xml',pretty_print=True)

#Command to get ouput
#print(client.service.GetAirConditionerData())
#print(client.service.AddAirConditionerData("108","19/09/1967 17:20","26.7","55.5"))
#print(client.service.GetAirConditionerDataByRoomNumber("108"))

#print(client.service.GetStudentData())
#print("stdName is "+str(type(client.service.GetStudentData()[0].stdName)))
#print("stdID is "+str(type(client.service.GetStudentData()[0].stdID)))
#print("stdHobby is "+str(type(client.service.GetStudentData()[0].stdHobby)))
#print("stdSport is "+str(type(client.service.GetStudentData()[0].stdSport)))
