﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://ts_company/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService{

    public WebService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public class AirConditioner
    {
        public string room_number;
        public double temperature;
        public double humidity;
        public string time;
    }

    public class StudentData
    {
        public string stdName;
        public string stdID;
        public string stdHobby;
        public string stdSport;
    }

    public class DeliveryData
    {
        public string dID;
        public string dName;
        public string dAddress;
        public float dWeight;
        public string dStatus;
    }

    // QUERY DATA OF STUDENT
    [WebMethod]
    public StudentData[] GetStudentData()
    {
        int index = 0;
        XElement xelement = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/student_data.xml");
        IEnumerable<XElement> stdInfo = xelement.Elements();
        StudentData[] StudentDataArray = new StudentData[stdInfo.Count()];
        foreach (var data in stdInfo)
        {
            StudentDataArray[index] = new StudentData()
            {
                stdName = (string)data.Element("Name"),
                stdID = (string)data.Element("ID"),
                stdHobby = (string)data.Element("Hobby"),
                stdSport = (string)data.Element("Sport")
            };
            index++;
        }
        return StudentDataArray;
    }

    // INSERT DATA OF DELIVERY
    [WebMethod]
    public string AddDeliveryData(string ctmName, string ctmAddr,
        float pkgWeight, string ctmID)
    {
        XDocument xmlDoc = XDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "/delivery.xml");
        xmlDoc.Elements("DeliveryData").First().Add(new XElement("Customer",
            new XElement("ID", ctmID),
            new XElement("Name", ctmName),
            new XElement("Address", ctmAddr),
            new XElement("WeightKg", pkgWeight),
            new XElement("Status", "Delivering")));
        xmlDoc.Save(AppDomain.CurrentDomain.BaseDirectory + "/delivery.xml");
        return "already done";
    }

    // CHANGE STATUS TO DELIVERED
    [WebMethod]
    public string Delivered(string ctmID)
    {
        XDocument xmlFile = XDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "/delivery.xml");
        XElement penItemValue = xmlFile
        .Elements("DeliveryData")
        .Elements("Customer")
        .Elements("Name")
        .Single(Name => Name.Value == ctmID)
        .Parent
        .Element("Status");
        penItemValue.Value = "Delivered";
        xmlFile.Save(AppDomain.CurrentDomain.BaseDirectory + "/delivery.xml");
        return "already done";
    }

    // GET DELIVERY HISTORY
    [WebMethod]
    public DeliveryData[] GetDeliveryData()
    {
        int index = 0;
        XElement xelement = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/delivery.xml");
        IEnumerable<XElement> dlvyData = xelement.Elements();
        DeliveryData[] dlvyDataArray = new DeliveryData[dlvyData.Count()];
        foreach (var data in dlvyData)
        {
            dlvyDataArray[index] = new DeliveryData()
            {
                dID = (string)data.Element("ID"),
                dName = (string)data.Element("Name"),
                dAddress = (string)data.Element("Address"),
                dStatus = (string)data.Element("Status"),
                dWeight = (float)data.Element("WeightKg")
            };
            index++;
        }
        return dlvyDataArray;
    }

    // GET DATA THAT PACKAGE IS ON DELIVERING
    [WebMethod]
    public DeliveryData[] GetOnDeliveringData()
    {
        int index = 0;
        int number = 0;
        XElement xelement = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/delivery.xml");
        IEnumerable<XElement> dlvyData = xelement.Elements();
        foreach (var data in dlvyData)
        {
            if ((string)data.Element("Status") == "Delivering") {
                number++;
            } else {
                index++;
            }
        }
        DeliveryData[] dlvyDataArray = new DeliveryData[number];
        if (number == 0){ return dlvyDataArray; }
        foreach (var data in dlvyData)
        {
            if ((string)data.Element("Status") == "Delivering")
            {
                dlvyDataArray[index] = new DeliveryData()
                {
                    dID = (string)data.Element("ID"),
                    dName = (string)data.Element("Name"),
                    dAddress = (string)data.Element("Address"),
                    dStatus = (string)data.Element("Status"),
                    dWeight = (float)data.Element("WeightKg")
                };
                index++;
            }
            else
            {
                index++;
            }
        }
        return dlvyDataArray;
    }

    // GET DATA THAT PACKAGE IS DELIVERED
    [WebMethod]
    public DeliveryData[] GetDeliveredData()
    {
        int index = 0;
        int number = 0;
        XElement xelement = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/delivery.xml");
        IEnumerable<XElement> dlvyData = xelement.Elements();
        foreach (var data in dlvyData)
        {
            if ((string)data.Element("Status") == "Delivered")
            {
                number++;
            } else {
                index++;
            }
        }
        DeliveryData[] dlvyDataArray = new DeliveryData[number];
        if (number == 0) { return dlvyDataArray; }
        foreach (var data in dlvyData)
        {
            if ((string)data.Element("Status") == "Delivered")
            {
                dlvyDataArray[index] = new DeliveryData()
                {
                    dID = (string)data.Element("ID"),
                    dName = (string)data.Element("Name"),
                    dAddress = (string)data.Element("Address"),
                    dStatus = (string)data.Element("Status"),
                    dWeight = (float)data.Element("WeightKg")
                };
                index++;
            }
            else
            {
                index++;
            }
        }
        return dlvyDataArray;
    }

    // INSERT DATA INTO AIR CONDITIONER XML FILE
    [WebMethod]
    public void AddAirConditionerData(string room_number, string time,
        double temperature, double humidity){
        XDocument xmlDoc = XDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "/air_data.xml");
        xmlDoc.Elements("AirConditioner").Last().Add(new XElement("AirData",
            new XElement("Room",room_number),
            new XElement("Time",time), 
            new XElement("Temperature", temperature), 
            new XElement("Humidity",humidity)));
        xmlDoc.Save(AppDomain.CurrentDomain.BaseDirectory + "/air_data.xml");
    }

    // QUERY DATA OF AIR CONDITIONER
    [WebMethod]
    public AirConditioner[] GetAirConditionerData()
    {
        int index = 0;
        XElement xelement = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/air_data.xml");
        IEnumerable<XElement> acInfo = xelement.Elements();
        AirConditioner[] ACDataArray = new AirConditioner[acInfo.Count()];
        foreach (var data in acInfo)
        {
            ACDataArray[index] = new AirConditioner() 
            {
                room_number = (string)data.Element("Room"),
                temperature = (double)data.Element("Temperature"),
                humidity = (double)data.Element("Humidity"),
                time = (string)data.Element("Time")
            };
            index++;
        }
        return ACDataArray;
    }

    // QUERT DATA OF AIR CONDITIONER BY ROOM NUMBER
    [WebMethod]
    public AirConditioner[] GetAirConditionerDataByRoomNumber(string roomNumber){
        int index = 0;
        XElement xelement = XElement.Load(AppDomain.CurrentDomain.BaseDirectory + "/air_data.xml");
        IEnumerable<XElement> acInfo = xelement.Elements();
        AirConditioner[] ACDataArray = new AirConditioner[1];
        foreach (var data in acInfo){
            if (roomNumber == (string)data.Element("Room")) {
                ACDataArray[0] = new AirConditioner(){
                    room_number = (string)data.Element("Room"),
                    temperature = (double)data.Element("Temperature"),
                    humidity = (double)data.Element("Humidity"),
                    time = (string)data.Element("Time")
                };
                break;
            } else { 
                index++;
            }
        }
        return ACDataArray;
    }
}
